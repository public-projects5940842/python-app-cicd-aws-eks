# python-app-cicd-aws-eks
![alt text](doc/images/gitlab-ci-cd-architecture.png)

## About
This project focus mostly on preparing Gitlab CI/CD pipeline for deploying in EKS cluster ToDo application written in Python Flask . CD part is implemented in push-based model by using Gitlab Agent for Kubernetes tool (instead of kubectl commands and passing credentials).
- CI/CD pipeline will run in Gitlab.com, Gitlab runners are deployed in EC2 instance. We use shell and docker executors on this gitlab runner. 
- Pipeline consists of:,
	Running simple unit, integration, SAST tests,
	Docker image and pushing it to container repository on gitlab.com
	Deploying image on AWS EKS cluster - test and production environments (different namespaces in the same k8s cluster)
- In k8s cluster it will be used (mostly by Helm Chart):
	External DNS to manage DNS on Route53
	Nginx Ingress Controller and Load Balancer
	Lets Encrypt for TLS (only for production environment)
	Prometheus for production environment
- Todo app is written in Python using Flask framework which allows to simple storing of notes.

## Description and explanation of chosen tools in the project:  
- eksctl - simple CLI tool, where we can focus only for aws eks cluster creation, without going deeper. Although it is very powerfull tool which is very often used in production environments
- Gitlab.com as CI/CD - Gitlab is one of most often used CI tools, using online instance is faster way of using it and focus on developing application
- Terraform - fast tool to provision infrastructure - EC2 instances for Gitlab runners
- Python - for writing scripts to manage Gitlab.com via RestAPI, e.g. register gitlab runner, list existing runners or creation of gitlab agent for k8s to securely connect to eks cluster and deploy resources
- Nginx Ingress Controller and L4 Load Balancer - to expose deployed application to the internet (LoadBalancer) and route that traffic to a given k8s resources (Ingress controller)
- Lets Encrypt - is free CA to issue TLS for domains
- Prometheus - will allows to capture metrics from my application ( ToDo app using flask exporter metrics and my custom metrics deployed using prometheus client library for Python)
- Gitlab Agent for K8s - it is push-based model (Of course GitOps have more advantages than disadvantages, but k8s environment is not complex, so it will be easy to restore,recreate etc.) we also issue commands e.g. `kubectl apply -f manifest.yaml` from my CI/CD, but more securely. Although best practice is to use GitOps approach for deploying k8s resourecs.
- Makefile - automation, we can specifiy list of (linux) commands and order of executing of them

## Prerequisites

- Install `eksctl` (https://eksctl.io/installation/)
- Install `terraform` (https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- Install `helm` (https://helm.sh/docs/intro/install/)

### Requirements
- Gitlab account
- AWS account for EKS cluster, Route53, EC2 instances, VPC etc.

## Getting Started
1. First we need fulfill all variables in:
- `Makefile.variables` (mostly for aws eks cluster)
- `variables.tf`(for gitlab runner)
- environment variables (for gitlab automation via RestAPI)
  - GITLAB_PROJECT_ID - Id of gitlab project (where this repo is actually hosted)
  - GITLAB_TOKEN - token which has permissions to manage a whole project (for automation, e.g. register gitlab runners)
- `.gitlab-ci.yml` 
- `.gitlab/agents/k8s-name/config.yaml ` - select your group and project
2. Let's create our environment for CI/CD pipeline, we must create Gitlab runners (ec2 instance) and k8s cluster (aws eks)
```
make create_all_from_eks_to_ext_dns # create k8s cluster
cd terraform && terraform apply # create runners to execute Pipeline tasks
```

Now we must create and deploy gitlab agent for k8s, to allow pipeline to deploy images in EKS cluster (push-based model)
```
make create_token_in_gitlab_for_k8s_cluster_connection 
```

(Optional) if we want to use one of ec2 instance as dev environment, where we will deploy application in docker-compose manner, then we must provide to gitlab.com ssh key, to allow connection from pipeline into ec2 instance.
```
make add_private_key_to_gitlab_variable 
# or manually:
curl --request PUT --header "PRIVATE-TOKEN: <gitlab-token> \
    "https://gitlab.com/api/v4/projects/<project-id>/variables/SSH_PRIVATE_KEY" --form "value=$(shell cat terraform/key.key)"
```

Install Prometheus from helm chart in order to allow Prometheus discover targets, we must label Prometheus CRs (e.g. serviceMonitor and servicePod) `release: kube-prometheus-stack`. (We can change way in values.yaml of this prometheus chart)

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install kube-prometheus-stack --create-namespace --namespace kube-prometheus-stack prometheus-community/kube-prometheus-stack
```
Install ingress for prometheus and serviceMonitor to observe our application in production environment.
Now prometheus is accessible in this URL: prometheus.zmyslony.ovh
```
kubectl apply -f kubernetes/prometheus/ingress.yaml
kubectl apply -f kubernetes/prometheus/prometheus-service-monitor.yaml
```
3. Now we can run pipeline

```
glab ci run
```
4. Clean-up
```
terraform destroy
make delete_eks_cluster
```
## Result of the project:
Application Todo accesible on prod.zmyslony.ovh with TLS certificate.
Test version of application accesible on test.zmyslony.ovh for UAT etc.

### Gitlab CI/CD:

![alt text](doc/images/pipeline-gitlab.png)

### Test environment:

![alt text](doc/images/app-http.png)




### Production environment:

![alt text](doc/images/app-tls.png)



### Prometheus:

![alt text](doc/images/prometheus.gif)


## Challanges
- In Dockerfile Python can be challenging. For example: if we want to use uwsgi server in our simple app, then Alpine is not enough, because it doesn't include `gcc` libraries etc. So we need to install manually, but it creates various errors. Our user must have home directory (e.g. /home/username) to install Flask. Compiled programming languages are indeed faster :)
## Roadmap
- Get docker image version of the app from python project version - pyproject.tom, not manually typing what version (1.4?) apply in the CI/CD pipeline
- Write build pipelines that publish your containers automatically on each git tag of your main branch. Make sure to follow semantic versioning.
- Separate volume (PVC) for postgresql - a separate database instead sqlite
- Apply flux or argoCD
- Other than CNI - Cilium?
- Vault CSI provider and concept of Secrets Store CSI Driver
- Self hosted Gitlab
- Rocket chat self hosted
- HPA KEDA
- webhooks to inform slack or Rocket chat
- Taskfile instead of Makefile
- Traefik instead of Nginx
- Istio or Linkerd for Observability, mTLS
- Tekton for CI/CD
- Dagger for CI/CD
- ELK, Opensearch or Splunk, loki

## Authors

**Rafał Zmyślony** 
