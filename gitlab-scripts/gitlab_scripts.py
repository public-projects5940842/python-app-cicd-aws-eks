import requests
from pprint import pprint
import subprocess
import os

# My custom gitlab script. Token are invalid ;) or may not?
_GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
project_id = os.environ.get('GITLAB_PROJECT_ID')

variable_name = 'SSH_PRIVATE_KEY'

def update_variable_in_gitlab(variable_name: str, new_variable_value: str):
    '''
    curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/variables/NEW_VARIABLE" --form "value=updated value"

    curl --request PUT --header "PRIVATE-TOKEN: glpat-wQbd2j_5_Hcyys8z3tf-" \
        "https://gitlab.com/api/v4/projects/48245022/variables/SSH_PRIVATE_KEY" --form "value=$(cat terraform/key.key)"
    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    endpoint_variables = 'https://gitlab.com/api/v4/projects/48245022/variables'

    data = {
        'value': new_variable_value
    }

    try:
        response = requests.get(endpoint_variables, headers=headers)
        for variable in response.json():
            if variable.get('key') == variable_name:
                url = f'{endpoint_variables}/{variable_name}'
                response = requests.put(url, headers=headers, data=data)
                print('Changed')
                print(response.json())

    except Exception as e:
        print('error')
        print(e)

    return response.status_code


def list_existing_runners():
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    endpoint_runners = 'https://gitlab.com/api/v4/runners'

    try:
        response = requests.get(endpoint_runners, headers=headers)
        print(response)
        for a in response.json():
            print(a)

    except Exception as e:
        print('error')
        print(e)

    return response.status_code


def remove_existing_runners():
    '''
    curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/6"

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    endpoint_runners = 'https://gitlab.com/api/v4/runners'

    try:
        response = requests.get(endpoint_runners, headers=headers)
        print(response)
        for runner in response.json():
            id = runner.get('id')
            print(id)
            url = f'{endpoint_runners}/{id}'
            response = requests.delete(url, headers=headers) # response return only status code 204 if SUCCESS
            print(111, response.status_code)

    except Exception as e:
        print('error')
        print(e)

    return response.status_code

def get_runner_token_from_project(project_id: str) -> str:
    '''
    In gitlab there are:
    - auth token
    - registered token   ---> to register runner for example

    This token in individually assinged for each Project

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    
    endpoint_runners = (f'https://gitlab.com/api/v4/projects/{project_id}')
    try:
        response = requests.get(endpoint_runners, headers=headers)
        print(response.json())
        token_from_given_project = response.json().get('runners_token')
        print('TOKEN --->>>>', token_from_given_project)
        return token_from_given_project

    except Exception as e:
        print('error')
        print(e)
        return None


def create_runner(tags: str, description: str, registration_token: str):
    ''' BETTER option is `create_runner_and_return_token`
    This function must be issued from actual server which is going to be Gitlab runner. Because, once it called, it is registered this OS/server
    which issued this command (in gitlab.com in runners section, IP address of the registered runner will appear)

    curl --request POST "https://gitlab.example.com/api/v4/runners" \
     --form "token=<registration_token>" --form "description=test-1-20150125-test" \
     --form "tag_list=ruby,mysql,tag1,tag2"

     curl --header "Private-Token: glpat-wQbd2j_5_Hcyys8z3tf-" "https://gitlab.com/api/v4/projects/48245022"
     WHERE:
     48245022 - project ID
     token - it is individual registration token (for runners) for each project. Each project has a such token

     response.json()  if success (201) gives
     {'id': 33258426, 'token': 'HeukenxsXLH-xBsF19Q7', 'token_expires_at': None}
     '''

    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    endpoint_runners = 'https://gitlab.com/api/v4/runners'

    data = {
        'description': description,
        'tag_list': tags,
        'token': registration_token,
    }

    try:
        response = requests.post(endpoint_runners, headers=headers, data=data)
        print(response.json())

    except Exception as e:
        print('error')
        print(e)

    return response.status_code



'''

'''
def create_runner_and_return_token(tags: str, description: str, project_id: str) -> str:
    ''' https://docs.gitlab.com/ee/tutorials/automate_runner_creation/?tab=Project

    it creates runner in gitlab.com --> settings - ci/cd - runners AND return token
    This token can be used to register Gitlab runner in some VM. No more manually clicking in UI, and copy & paste returned code
    
    With returned token as $RUNNER_TOKEN, we can use this token, to actually register a given Runner to a specific Server (executor)
    (associate runner with actual server)
    
    gitlab-runner register --non-interactive --name="$GL_NAME" --url="https://gitlab.com"
  --token="$RUNNER_TOKEN" --request-concurrency="12" --executor="docker"
  --docker-image="python:alpine3.18"

    response.json() gives
    {'id': 33269277, 'token': 'glrt-tzHKjGCdHSzNnPwEvjt8', 'token_expires_at': None}
    the same but using curl
# CURL

curl --request POST --url "https://gitlab.com/api/v4/user/runners" \
--data "runner_type=project_type" \
--data "project_id=48245022" \
--data "description=aaaaaa" \
--data "tag_list=ec2,shell" \
--header "PRIVATE-TOKEN: glpat-wQbd2j_5_Hcyys8z3tf-"

# curl and export returned token to $RUNNER_TOKEN

response=$(curl --request POST --url "https://gitlab.com/api/v4/user/runners" \
--data "runner_type=project_type" \
--data "project_id=48245022" \
--data "description=aaaaaa" \
--data "tag_list=ec2,shell" \
--header "PRIVATE-TOKEN: glpat-wQbd2j_5_Hcyys8z3tf-"); echo $response | jq .token > temp_token.txt; \
export RUNNER_TOKEN=$(cat temp_token.txt) && rm temp_token.txt

gitlab-runner register --non-interactive --name="$GL_NAME" --url="https://gitlab.com"
  --token="$RUNNER_TOKEN" --request-concurrency="12" --executor="docker"
  --docker-image="python:alpine3.18"
     '''

    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    endpoint_runners = 'https://gitlab.com/api/v4/user/runners'

    data = {
        'description': description,
        'tag_list': tags,
        'runner_type': 'project_type', # this is project runners, there are also group runners...
        'project_id': project_id
    }

    try:
        response = requests.post(endpoint_runners, headers=headers, data=data)
        print(response.json())
        return response.json().get('token') 

    except Exception as e:
        print('error')
        print(e)



# create_runner('ec2, docker,shell', 'test_desc', get_runner_token_from_project(project_id))
# list_existing_runners()
# remove_existing_runners()
        
# a= create_runner_and_return_token('ec2, docker,shell', 'eeeee', project_id)
# print(a)

def get_agents_in_project(project_id: str) -> list:
    '''
    response.json() returns list of dict

    curl --header "PRIVATE-TOKEN: glpat-wQbd2j_5_Hcyys8z3tf-" "https://gitlab.com/api/v4/projects/"

    curl --header "PRIVATE-TOKEN: glpat-wQbd2j_5_Hcyys8z3tf-" \
      "https://gitlab.com/api/v4/projects/20/cluster_agents"

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    
    endpoint_runners = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents')
    try:
        response = requests.get(endpoint_runners, headers=headers)
        response = response.json()
        print('####################################################')
        pprint(response)
        print('####################################################')
        return response

    except Exception as e:
        print('error')
        print(e)
        return None

def list_agent_token_in_project(project_id: str) -> list:
    '''
    response.json() returns list of dict

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }

    agent_id = get_agents_in_project(project_id)[0].get('id')
    endpoint = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents/{agent_id}/tokens')
    response = requests.get(endpoint, headers=headers)
    print(response.json())
    return response.json()

def remove_all_agent_token_in_project(project_id: str) -> list:
    '''
    response.json() returns list of dict

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }

    agent_id = get_agents_in_project(project_id)[0].get('id')
    endpoint = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents/{agent_id}/tokens')
    response = requests.get(endpoint, headers=headers)
    if not response.json():
        return None
    
    for agent in response.json():
        id = agent.get('id')
        print(id)
        endpoint = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents/{agent_id}/tokens/{id}')
        response = requests.delete(endpoint, headers=headers)
        if response.status_code == 204:
            print('Sucess')
    return response.status_code


def create_agent_token_in_project(project_id: str, new_token_name: str) -> dict:
    '''
    !!! Error, with already one agent is active
    if success - 201
    response.json() return dict
    sample response:
    {'agent_id': 1074231,
    'created_at': '2024-03-02T11:36:44.898Z',
    'created_by_user_id': 15338982,
    'description': None,
    'id': 1095158,
    'last_used_at': None,
    'name': '24124124',
    'status': 'active',
    'token': 'glagent-ApQJ7nYBN3XUVauDx_AtbsVPBxaMHxMSXKo31s7fxBL3x6YyBg'}
    
    In curl manner:
    curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/tokens" \
    -H "Content-Type:application/json" \
    -X POST --data '{"name":"some-token"}'

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }
    data = {
        'name': new_token_name
    }
    agent_id = get_agents_in_project(project_id)[0].get('id')
    endpoint = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents/{agent_id}/tokens')

    try:
        response = requests.post(endpoint, headers=headers, data=data)
        if create_agent_token_in_project == 400:
            print(f'ERROR:', response.json().get('message'))
            return response.json().get('message')
        
        if response.status_code == 201:
            token = response.json().get('token')
            cmd1 = 'helm repo add gitlab https://charts.gitlab.io'
            cmd2 = 'helm repo update'
            cmd3 = f"""helm upgrade --install k8s-name gitlab/gitlab-agent \\
    --namespace gitlab-agent-k8s-name \\
    --create-namespace \\
    --set image.tag=v16.10.0-rc1 \\
    --set config.token={token} \\
    --set config.kasAddress=wss://kas.gitlab.com"""
            subprocess.run(cmd1, shell=True, check=True)
            subprocess.run(cmd2, shell=True, check=True)
            subprocess.run(cmd3, shell=True, check=True)

        print('####################################################')
        pprint(response.json())
        print('####################################################')
        return response.json()

    except Exception as e:
        print('error')
        print(e)
        return None
    
def dev_list_agent_token_in_project(project_id: str) -> dict:
    '''
    !!! Error, with already one agent is active
    if success - 201
    response.json() return dict
    sample response:
    {'agent_id': 1074231,
    'created_at': '2024-03-02T11:36:44.898Z',
    'created_by_user_id': 15338982,
    'description': None,
    'id': 1095158,
    'last_used_at': None,
    'name': '24124124',
    'status': 'active',
    'token': 'glagent-ApQJ7nYBN3XUVauDx_AtbsVPBxaMHxMSXKo31s7fxBL3x6YyBg'}
    
    In curl manner:
    curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/tokens" \
    -H "Content-Type:application/json" \
    -X POST --data '{"name":"some-token"}'

    '''
    headers = {
        "PRIVATE-TOKEN": f"{_GITLAB_TOKEN}"
    }

    agent_id = get_agents_in_project(project_id)[0].get('id')
    endpoint = (f'https://gitlab.com/api/v4/projects/{project_id}/cluster_agents/{agent_id}')

    
    response = requests.get(endpoint, headers=headers)
    a = response
    print(a)
    print(get_agents_in_project(project_id))

    

# list_agent_token_in_project(project_id)
# get_agents_in_project(project_id)
# create_agent_token_in_project(project_id, 'a')
# list_agent_token_in_project(project_id)
# remove_all_agent_token_in_project(project_id)
# get_runner_token_from_project(project_id)
# list_existing_runners()
