run.py is for running CLI for functions from gitlab_scripts.py
It takes 3 arguments - optionals
Example:
```
python3 gitlab_scripts/run.py create_agent_token_in_project 48245022 aaa
```
gitlab_scripts.py contains python function to manage Gitlab. To automate boring stuff
examples:
    - create token to Gitlab Agent for K8s cluster
    - list projects in my Gitlab.com account