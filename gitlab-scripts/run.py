import argparse
import gitlab_scripts

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                        prog='Gitlab scripts to manage project',
                        description='Use this python scripts in CLI manner',
                        epilog='Initially it was doc file, to collect my notes. Now it is program to automate my workflows in gitlab')

    parser.add_argument('function_name', help='Function name from the script', nargs='?')  
    parser.add_argument('arg1', help='First argument of the function', default=None, nargs='?')  
    parser.add_argument('arg2', help='Second argument of the function', default=None, nargs='?')  

    arguments_from_cli = parser.parse_args()

    if arguments_from_cli.function_name == 'create_agent_token_in_project':
        gitlab_scripts.create_agent_token_in_project(arguments_from_cli.arg1, arguments_from_cli.arg2)
    elif arguments_from_cli.function_name == 'remove_all_agent_token_in_project':
        # argL PROJECT_ID
        gitlab_scripts.remove_all_agent_token_in_project(arguments_from_cli.arg1)  
    elif arguments_from_cli.function_name == 'remove_existing_runners':
        gitlab_scripts.remove_existing_runners()
    elif arguments_from_cli.function_name == 'remove_existing_agent_tokens':
        gitlab_scripts.remove_all_agent_token_in_project(arguments_from_cli.arg1)
    else:
        print('You did not pass any arguments. Maybe use gitlab_scripts.py directly?')
