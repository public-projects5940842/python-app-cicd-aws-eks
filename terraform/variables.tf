variable "gitlab_personal_token" {
  type = string
  default = "1234567"
  description = "Token to interact with Gitlab API to create and register runner in gitlab.com"
}

variable "gitlab_project_id" {
  type = string
  default = "1234567"
  description = "This is ID of the project, where runner will be applied"
}
