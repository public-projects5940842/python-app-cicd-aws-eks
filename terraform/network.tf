#==============VPC and network==============
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  
}

resource "aws_subnet" "public_app" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "public subnet"
  }
}

resource "aws_internet_gateway" "project1_igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "igw_for_vpc"

  }
}

resource "aws_route_table" "project1_public_rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.project1_igw.id
  }
}
# assign route table for subnet --> association
resource "aws_route_table_association" "public" {
  route_table_id = aws_route_table.project1_public_rt.id
  subnet_id = aws_subnet.public_app.id
}

#============== Security Groups==========================
# 1. todo app
resource "aws_security_group" "runner" {
  depends_on = [aws_vpc.main]
  vpc_id = "${aws_vpc.main.id}"
  name_prefix = "todo_app"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

#  port 8080 for todo app
    ingress {
    from_port = 1024
    to_port = 1048
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 20
    to_port = 21
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8082
    to_port = 8082
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }

}
