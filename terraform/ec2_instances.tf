resource "aws_instance" "ec2_runner" {
    instance_type = "t2.micro"
    depends_on = [aws_subnet.public_app]
    ami = "ami-04e601abe3e1a910f"
    key_name      = aws_key_pair.generated_key.key_name
    vpc_security_group_ids = [
        aws_security_group.runner.id
    ]
    subnet_id = aws_subnet.public_app.id
    associate_public_ip_address = true
    tags = {
        Name = "gitlab runner"
        Version: 1
    }

    user_data = <<EOF
#!/bin/bash

apt-get -y update
apt -y install net-tools
apt -y install python3-pip
apt -y install python3.10-venv
apt-get -y update
apt-get -y install libpq-dev libpq-dev python3-dev
apt-get -y install postgresql
apt-get -y install gcc
apt install jq -y

apt -y update
apt -y install build-essential
wget https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
export GOPATH=/usr/local/go/bin
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
apt install gitlab-runner -y

apt-get -y update
apt-get -y install ca-certificates curl gnupg
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get -y update
apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# KUBECTL
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

groupadd docker
usermod -aG docker ubuntu
newgrp docker
chown ubuntu:docker /var/run/docker.sock
chmod 664 /var/run/docker.sock

usermod -aG docker gitlab-runner
chown gitlab-runner:docker /var/run/docker.sock
chmod 664 /var/run/docker.sock



curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get -y  install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get -y update
sudo apt-get -y install helm


##############################################################
# Non-interactive of creating Gitlab runner from this VM
##############################################################
# 1. Create gitlab runner in gitlab.com

# For Docker executor

response=$(curl --request POST --url "https://gitlab.com/api/v4/user/runners" \
--data "runner_type=project_type" \
--data "project_id=${var.gitlab_project_id}" \
--data "description=Docker executor" \
--data "tag_list=ec2,docker" \
--header "PRIVATE-TOKEN: ${var.gitlab_personal_token}"); echo $response | jq .token | sed -e 's/"//g' > temp_token.txt; \
export RUNNER_TOKEN=$(cat temp_token.txt) && rm temp_token.txt

# 2. Register this VM as Gitlab runner.

gitlab-runner register --non-interactive --name="111docker111" --url="https://gitlab.com" \
  --token="$RUNNER_TOKEN" --executor="docker" \
  --docker-image="python:alpine3.18"

### The same two steps for Shell executor:

response=$(curl --request POST --url "https://gitlab.com/api/v4/user/runners" \
--data "runner_type=project_type" \
--data "project_id=${var.gitlab_project_id}" \
--data "description=Shell executor" \
--data "tag_list=ec2,shell" \
--header "PRIVATE-TOKEN: ${var.gitlab_personal_token}"); echo $response | jq .token | sed -e 's/"//g' > temp_token.txt; \
export RUNNER_TOKEN=$(cat temp_token.txt) && rm temp_token.txt

# 2. Register this VM as Gitlab runner.


gitlab-runner register --non-interactive --name="111shell111" --url="https://gitlab.com" \
--token="$RUNNER_TOKEN" --executor="shell"

gitlab-runner run
EOF
}

# VM for deploying containers

# resource "aws_instance" "dev_environment" {
#     instance_type = "t2.micro"
#     depends_on = [aws_subnet.public_app]
#     ami = "ami-04e601abe3e1a910f"
#     key_name      = aws_key_pair.generated_key.key_name
#     vpc_security_group_ids = [
#         aws_security_group.runner.id
#     ]
#     subnet_id = aws_subnet.public_app.id
#     associate_public_ip_address = true
#     tags = {
#         Name = "dev environment"
#         Version: 1
#     }

#     user_data = <<EOF
# #!/bin/bash

# apt-get -y update
# apt -y install net-tools
# apt -y install python3-pip
# apt -y install python3.10-venv
# apt-get -y update
# apt-get -y install libpq-dev libpq-dev python3-dev
# apt-get -y install postgresql
# apt-get -y install gcc

# apt -y update
# apt -y install build-essential
# wget https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
# rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
# export PATH=$PATH:/usr/local/go/bin
# export GOPATH=/usr/local/go/bin

# apt-get -y update
# apt-get -y install ca-certificates curl gnupg
# install -m 0755 -d /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# chmod a+r /etc/apt/keyrings/docker.gpg
# echo \
#   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
#   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#   tee /etc/apt/sources.list.d/docker.list > /dev/null
# apt-get -y update
# apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
# curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
# install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# groupadd docker
# usermod -aG docker ubuntu
# newgrp docker
# sudo chown ubuntu:docker /var/run/docker.sock
# apt install docker-compose -y
# EOF
# }

resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = "2048"
}

resource "local_file" "ssh_key" {
  content         = tls_private_key.ssh_key.private_key_pem
  filename        = "${path.module}/key.key"
  file_permission = 0400
}
resource "aws_key_pair" "generated_key" {
  key_name   = "gitlab"
  public_key = tls_private_key.ssh_key.public_key_openssh
}
