# Aws eks cluster info
CLUSTER_NAME := todo-app-v1
EC2_TYPE := t2.micro
PUBLIC_KEY_PATH := 
NODE_NUMBER := 2
REGION := eu-central-1

# Aws info
EXTERNAL_DNS := 
ACCOUNT_ID := 
ARN_IAM_POLICY_FOR_ROUTE_53 := 
SERVICE_ACCOUNT_NAME_DNS := 

# external dns
HOSTED_ZONE_IDENTIFIER := 
HOSTED_ZONE_NAME := 
NAMESPACE := 

# Gitlab token
GITLAB_TOKEN := 
GITLAB_PROJECT_ID := 

include Makefile.variable
create_eks_cluster:
	eksctl create cluster --name ${CLUSTER_NAME} --region ${REGION} \
       	--nodegroup-name linux-group-nodes --node-type ${EC2_TYPE} \
		--nodes ${NODE_NUMBER} --node-volume-size 30 --ssh-access --ssh-public-key ${PUBLIC_KEY_PATH}

install_nginx_ingress_controller_helm:
	helm upgrade --install ingress-nginx ingress-nginx \
    	--repo https://kubernetes.github.io/ingress-nginx \
    	--namespace ingress-nginx --create-namespace

enable_oidc_provider_for_eks:
	eksctl utils associate-iam-oidc-provider --region=${REGION} --cluster=${CLUSTER_NAME} --approve

create_iam_service_account_for_external_dns:
	eksctl -v 10 create iamserviceaccount --name ${SERVICE_ACCOUNT_NAME_DNS} --cluster ${CLUSTER_NAME} \
		--region ${REGION} --attach-policy-arn ${ARN_IAM_POLICY_FOR_ROUTE_53} --override-existing-serviceaccounts --approve 

install_external_dns_helm:
	helm upgrade --install my-release \
		--set provider=aws \
		--set aws.zoneType=public \
		--set txtOwnerId=${HOSTED_ZONE_IDENTIFIER} \
		--set domainFilters[0]=${HOSTED_ZONE_NAME} \
		--set serviceAccount.name=${SERVICE_ACCOUNT_NAME_DNS} \
		--set serviceAccount.create=false \
		bitnami/external-dns
		# oci://registry-1.docker.io/bitnamicharts/external-dns


delete_eks_cluster:
	eksctl delete cluster --name ${CLUSTER_NAME} --region ${REGION}

create_all_from_eks_to_ext_dns: create_eks_cluster install_nginx_ingress_controller_helm enable_oidc_provider_for_eks \
	create_iam_service_account_for_external_dns install_external_dns_helm install_cert_manager

install_gitlab_k8s_agent:
	@echo "First you must create Operate -> Kubernetes Clusters -> Create cluster to generate Gitlab Agent for K8s"
	@echo "If you have existing command to install a such agent, paste below"
	@echo ""
	helm repo add gitlab https://charts.gitlab.io
	helm repo update
	helm upgrade --install k8s-name gitlab/gitlab-agent \
    --namespace gitlab-agent-k8s-name \
    --create-namespace \
    --set image.tag=v16.5.0-rc2 \
    --set config.token=glagent-dsbC7zDUM5tSRgKUrxA6vv_pssbgjq2ipri5KdK6QAQP5tJK8A \
    --set config.kasAddress=wss://kas.gitlab.com

install_cert_manager:
	helm install \
	cert-manager jetstack/cert-manager \
	--namespace cert-manager \
	--create-namespace \
	--version v1.13.1 \
	--set installCRDs=true

remove_all_agent_token_in_project:
	python3 gitlab_scripts/run.py remove_all_agent_token_in_project 48245022 

remove_existing_runners:
	python3 gitlab_scripts/run.py remove_existing_runners

create_token_in_gitlab_for_k8s_cluster_connection:
    #  this command produces also helm chart with a new generated token
	python3 gitlab-scripts/run.py remove_existing_agent_tokens 48245022
	python3 gitlab-scripts/run.py create_agent_token_in_project 48245022 token_name

add_private_key_to_gitlab_variable:
	curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables/SSH_PRIVATE_KEY" --form "value=$(shell cat terraform/key.key)"


