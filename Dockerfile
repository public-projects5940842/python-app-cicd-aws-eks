FROM python:alpine3.18 as base
ARG USERNAME=application-todo
# Install system dependencies
RUN apk add --update --no-cache libc-dev linux-headers gcc pcre-dev

# Optional For debugging purposes
RUN apk add procps
RUN apk add net-tools
# Setup user
ARG UID=1000
ARG GID=1000
ARG USERNAME=application
RUN addgroup -S "$USERNAME" 
RUN adduser --uid "$UID" --disabled-password -G "$USERNAME" "$USERNAME" 

# Set up the application directory
WORKDIR /app
COPY /app/flask_app.py .
COPY /app/tests /app/tests
COPY /app/templates /app/templates
COPY /app/uwsgi.ini .
COPY /app/wsgi.py .
COPY requirements.txt .
RUN chown -R "$USERNAME":"$USERNAME" /app

# Install Python dependencies as root
RUN pip3 install --only-binary :all: Flask-SQLAlchemy greenlet
RUN pip3 install -r requirements.txt
RUN pip3 install uwsgi

# Switch to non-root user
USER "${USERNAME}"

# Environment setup for Prometheus (if necessary)
RUN mkdir PROMETHEUS_MULTIPROC_DIR
ENV PROMETHEUS_MULTIPROC_DIR=/app/PROMETHEUS_MULTIPROC_DIR

EXPOSE 5000
CMD ["uwsgi", "--ini", "/app/uwsgi.ini"]
# CMD ["uwsgi", "--ini", "/app/uwsgi.ini", "--socket", "/tmp/app.socket"]
