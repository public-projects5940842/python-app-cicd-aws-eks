
```
from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from prometheus_client import start_http_server, Summary, Gauge, Counter
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import generate_latest, CollectorRegistry
from prometheus_client.multiprocess import MultiProcessCollector
```

#### Notice difference between prometheus_client and prometheus_flask_exporter.
For example in Prometheus specification for client libraries for diffrent languages in https://prometheus.io/docs/instrumenting/writing_clientlibs/

E.g.  why `CollectorRegistry`?
The key class is the Collector. This has a method (typically called ‘collect’) that returns zero or more metrics and their
samples. Collectors get registered with a CollectorRegistry. 
Data is exposed by passing a CollectorRegistry to a class/method/function "bridge", which returns the metrics
in a format Prometheus supports. Every time the CollectorRegistry is scraped it must callback to each of the Collectors’
collect method

In from prometheus_flask_exporter import PrometheusMetrics we have some default methods (4 types).
In this program we have these default metrics as my custom metrics.

For multiprocess applications (WSGI or otherwise), you can find some helper classes in the prometheus_flask_exporter.multiprocess module. 
These provide convenience wrappers for exposing metrics in an environment where multiple copies of the application will run on a single host.
If you're simply looking to expose metrics from a Flask app, using PrometheusMetrics(app) alone should suffice for most applications. This setup automatically handles the /metrics endpoint and provides a straightforward way to instrument your Flask routes and other metrics without needing to manage the registry and collector manually.
Only resort to manual CollectorRegistry management and defining a custom /metrics route if your application's architecture specifically requires it, such as in a multi-process Flask application where metrics from all processes need to be aggregated.
'''


### PROMETHEUS_MULTIPROC_DIR
I expect that PROMETHEUS_MULTIPROC_DIR should be cleaned at startup of your application to handle odd case where the application previous run was not able to do so.

in this folder is all metrics, so if we invoke /metrics endpoint it will present metrics that are coming from this directory (files are in .db extension, so we can even move metrics from another application and it will be presented.)

```
counter_1001.db  counter_589.db     gauge_all_1416.db  gauge_max_1001.db  gauge_max_953.db   summary_1079.db  summary_590.db
counter_1079.db  counter_590.db     gauge_all_1719.db  gauge_max_1079.db  histogram_1001.db  summary_1121.db  summary_702.db
```  

### Name conventions
Notice that some metrics must end with `_total` e.g. `flask_http_request_exceptions_total` or `sum` like in `flask_http_request_duration_seconds_sum`.

### 4 types of metrics have diffrent function, like inc(), dec(), observe()

### Examples of defining metrics

```
            c = Counter('my_requests_total', 'HTTP Failures', ['method', 'endpoint'])
            c.labels('get', '/').inc()
            c.labels('post', '/submit').inc()
```
