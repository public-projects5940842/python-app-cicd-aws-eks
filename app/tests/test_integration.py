import pytest
from flask_app import app, db, Todo

@pytest.fixture
def client():
    app.config["TESTING"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
    client = app.test_client()

    with app.app_context():
        db.create_all()
        yield client
        db.session.remove()
        db.drop_all()
@pytest.mark.filterwarnings("ignore::sqlalchemy.exc.SADeprecationWarning")
def test_index(client):
    response = client.get("/it-works")
    assert b"Hello - it is working" in response.data
@pytest.mark.filterwarnings("ignore::sqlalchemy.exc.SADeprecationWarning")
def test_add_todo(client):
    response = client.post("/add", data={"task": "Test task"})
    assert response.status_code == 302  # Redirect after adding a todo
    todos = db.session.query(Todo).all()  # Use db.session.query()
    assert len(todos) == 1
    assert todos[0].task == "Test task"
    assert not todos[0].completed
@pytest.mark.filterwarnings("ignore::sqlalchemy.exc.SADeprecationWarning")
def test_complete_todo(client):
    todo = Todo(task="Test task")
    db.session.add(todo)
    db.session.commit()
    response = client.get(f"/complete/{todo.id}")
    assert response.status_code == 302  # Redirect after completing a todo
    assert todo.completed
@pytest.mark.filterwarnings("ignore::sqlalchemy.exc.SADeprecationWarning")
def test_delete_todo(client):
    todo = Todo(task="Test task")
    db.session.add(todo)
    db.session.commit()
    response = client.get(f"/delete/{todo.id}")
    assert response.status_code == 302  # Redirect after deleting a todo
    todos = db.session.query(Todo).all()  # Use db.session.query()
    assert len(todos) == 0

if __name__ == "__main__":
    pytest.main()
