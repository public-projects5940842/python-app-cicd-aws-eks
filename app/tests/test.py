import unittest
from flask_app import app, db, Todo

class TestApp(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()
        with app.app_context():
            db.create_all()

    def test_index_route(self):
        response = self.client.get('/it-works')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Hello - it is working', response.data)

if __name__ == '__main__':
    unittest.main()
