from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from prometheus_client import start_http_server, Summary, Gauge, Counter
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import generate_latest, CollectorRegistry
from prometheus_client.multiprocess import MultiProcessCollector

import time

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///todo.db"
db = SQLAlchemy(app)

# Let's use object coming from Flask Exporter - it provides 4 default metrics
metrics_object = PrometheusMetrics(app)

# Here are object coming from Prometheus library - not Flask Exporter
registry = CollectorRegistry()
MultiProcessCollector(registry)

# My custom metrics
ITEMS_IN_DATABASE = Summary('todo_items_in_database', 'The total number of items in the database')
NEW_TODO_ITEM_COUNT_IN_SESSION = Counter('new_item_count_in_session', 'How many new items was created in a given session')
NEW_TODO_ITEM_CREATION_LATENCY = Gauge('new_item_creation_latency', 'How much time has passed between displaying app ("/")\
                                        and adding a new item ("/add")', ['label_A'])
MEMORY_USAGE = Gauge('memory_usage_random_value', 'it is made up metric, value is generated radomly')
SILLY_GAUGE_METRIC = Gauge('silly_metric', 'Silly metric, when you add item +1, remove -10, start value is 100')

# init some metrics values
SILLY_GAUGE_METRIC.set(100)
start_counting_time = 0
end_counting_time = 0

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Boolean, default=False)

@app.route("/")
def index():
    global start_counting_time
    start_counting_time = time.time()
    todos = Todo.query.all()
    return render_template("index.html", todos=todos)


@app.route("/it-works")
def is_working():
    todos = Todo.query.all()
    return "Hello - it is working"


@app.route("/add", methods=["POST"])
def add_todo():
    NEW_TODO_ITEM_COUNT_IN_SESSION.inc()
    SILLY_GAUGE_METRIC.inc()
    task = request.form["task"]
    new_todo = Todo(task=task)
    db.session.add(new_todo)
    db.session.commit()

    global end_counting_time
    global start_counting_time
    end_counting_time = time.time()
    NEW_TODO_ITEM_CREATION_LATENCY.labels(label_A='bbb').set(end_counting_time - start_counting_time)
    start_counting_time = 0
    start_counting_time = 0
    return redirect(url_for("index"))


@app.route("/complete/<int:todo_id>")
def complete(todo_id):
    
    todo = Todo.query.get(todo_id)
    todo.completed = True
    db.session.commit()
    return redirect(url_for("index"))


@app.route("/delete/<int:todo_id>")
def delete(todo_id):
    todo = Todo.query.get(todo_id)
    db.session.delete(todo)
    db.session.commit()
    SILLY_GAUGE_METRIC.dec(10)
    return redirect(url_for("index"))

@app.route('/metrics')
def metrics():

    todos = Todo.query.all()
    count_of_todo_items = len(todos)
    ITEMS_IN_DATABASE.observe(count_of_todo_items)
    data = generate_latest(registry)
    return data


if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    app.run(debug=True,host="0.0.0.0")
