import logging
from flask_app import app, db

logging.basicConfig(level=logging.INFO)

def create_app():
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///instance/todo.db"
    with app.app_context():
        logging.info("Creating tables...")
        db.create_all()
    return app

# Ensure uWSGI can find 'application' as specified in uwsgi.ini
application = create_app()

if __name__ == '__main__':
    logging.info("Starting the application...")
    application.run()

